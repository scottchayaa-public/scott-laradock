# Scott Laradock

Use this light php docker enviriment.  
It was built with php alpine version.  
So the image and container size will smaller than laradock.  

# env

```sh
cp  .env.example .env
```

# nginx

```sh
cp nginx/sites/laravel.conf.example nginx/sites/laravel.conf
vi nginx/sites/laravel.conf

# should modify : server_name
```


# php-worker

```sh
# see php-worker/supervisor.d/laravel-worker.conf.example
```


# Run docker-compose

```
docker-compose up -d
```


# Troubleshooting #1

如果運行 php-worker 和 php-fpm 時出現錯誤  
使用 `docker-compose logs` 查看發現 :  

```
standard_init_linux.go:211: exec user process caused
```

表示 `init-shell` 裡檔案的「換行」儲存格是不正確  
`windows` 採用的形式 : `CRLF` 就是 `\r\n`  
而所有的 `UNIX` 系統 : 都是用 `LF` 就是 `\n`  
  
請確認 *.sh 儲存的格式是否為 `LF`  
然後再重新編譯 docker image : `docker-compose build __service__`
