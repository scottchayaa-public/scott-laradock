#!/bin/sh

chmod 777 /usr/local/src/init-shells/*.sh

for file in /usr/local/src/init-shells/*.sh
do
  if [ -f "$file" ]; then
    echo "sh $file"
    sh $file
  fi
done

/usr/bin/supervisord -n -c /etc/supervisord.conf

exec "$@"