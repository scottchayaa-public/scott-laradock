#!/bin/sh
set -e

for file in /usr/local/src/init-shells/*
do
  if [ -f "$file" ]; then
    echo "sh $file"
    sh $file
  fi
done

php-fpm

exec "$@"